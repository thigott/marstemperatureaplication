package com.example.marstemperature.network.retrofit

import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {

    @GET("insight_weather/")
    suspend fun getMarsTemperature(
        @QueryMap params: Map<String, String?>
    ): String
}