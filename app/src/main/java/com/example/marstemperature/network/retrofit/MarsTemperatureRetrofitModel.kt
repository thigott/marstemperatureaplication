package com.example.marstemperature.network.retrofit

import androidx.annotation.Keep
import com.example.marstemperature.utils.codec
import com.example.marstemperature.utils.fromJsonHelper
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@Keep
@JsonIgnoreProperties(ignoreUnknown = true)
data class MarsTemperatureRetrofitModel(

    @get:JsonProperty("PRE")@field:JsonProperty("PRE")
    val temperatureObject: TemperatureObject? = null,

    @get:JsonProperty("First_UTC")@field:JsonProperty("First_UTC")
    val firstUpdate: String? = null,

    @get:JsonProperty("Last_UTC")@field:JsonProperty("Last_UTC")
    val lastUpdate: String? = null,

    @get:JsonProperty("Season")@field:JsonProperty("Season")
    val season: String? = null
) {
    fun toJson() = codec.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = fromJsonHelper<MarsTemperatureRetrofitModel>(json)
    }
}

@Keep
@JsonIgnoreProperties(ignoreUnknown = true)
data class TemperatureObject(

    @get:JsonProperty("av")@field:JsonProperty("av")
    val averageTemperature: Double? = null,

    @get:JsonProperty("mn")@field:JsonProperty("mn")
    val minTemperature: Double? = null,

    @get:JsonProperty("mx")@field:JsonProperty("mx")
    val maxTemperature: Double? = null
)