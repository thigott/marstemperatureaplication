package com.example.marstemperature.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.marstemperature.network.retrofit.MarsTemperatureRetrofitModel
import com.example.marstemperature.repository.MarsTemperatureRepository
import com.example.marstemperature.utils.DataState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainViewModel
@ViewModelInject
constructor(
    private val marsTemperatureRepository: MarsTemperatureRepository
): ViewModel() {

    val temperatureDataState: MutableLiveData<DataState<ArrayList<MarsTemperatureRetrofitModel>>> = MutableLiveData()

    fun getMarsTemperature(
        params: Map<String, String>
    ){
        viewModelScope.launch {
            marsTemperatureRepository.getMarsTemperature(
                params = params
            ).collect { dataState ->
                temperatureDataState.value = dataState
            }
        }
    }
}