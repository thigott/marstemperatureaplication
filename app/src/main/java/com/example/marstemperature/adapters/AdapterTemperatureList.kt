package com.example.marstemperature.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.marstemperature.R
import com.example.marstemperature.network.retrofit.MarsTemperatureRetrofitModel
import com.example.marstemperature.utils.convertDate
import com.example.marstemperature.utils.formatTemperature
import kotlinx.android.synthetic.main.adapter_temperature_itemview.view.*

class AdapterTemperatureList
constructor(
    private val dataList: ArrayList<MarsTemperatureRetrofitModel>
): RecyclerView.Adapter<AdapterTemperatureList.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_temperature_itemview, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data = dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val date: TextView = itemView.tv_date
        private val minTemperature: TextView = itemView.tv_temperature_min
        private val maxTemperature: TextView = itemView.tv_temperature_max
        private val averageTemperature: TextView = itemView.tv_temperature_average

        @SuppressLint("SetTextI18n")
        fun bind(data: MarsTemperatureRetrofitModel){
            date.text = data.lastUpdate?.convertDate()
            minTemperature.text = "Min: ${data.temperatureObject?.minTemperature?.formatTemperature()}ºC"
            maxTemperature.text = "Max: ${data.temperatureObject?.maxTemperature?.formatTemperature()}ºC"
            averageTemperature.text = "Média: ${data.temperatureObject?.averageTemperature?.formatTemperature()}ºC"
        }
    }
}