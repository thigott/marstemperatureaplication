package com.example.marstemperature.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.marstemperature.R
import com.example.marstemperature.adapters.AdapterTemperatureList
import com.example.marstemperature.utils.Constants
import com.example.marstemperature.utils.DataState
import com.example.marstemperature.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.main_fragment.*

@AndroidEntryPoint
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel by viewModels<MainViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeObservers()
        viewModel.getMarsTemperature(
            params = mapOf(
                "api_key" to Constants.apiKey,
                "feedtype" to "json",
                "ver" to "1.0"
            )
        )
    }

    private fun subscribeObservers(){
        viewModel.temperatureDataState.observe(viewLifecycleOwner, Observer { dataState ->
            when (dataState) {
                is DataState.Success -> {
                    progress_bar.visibility = View.GONE

                    rv_temperature.apply {
                        visibility = View.VISIBLE
                        layoutManager = GridLayoutManager(context, 2)
                        adapter = AdapterTemperatureList(dataList = dataState.data)
                    }
                }

                is DataState.Loading -> {
                    progress_bar.visibility = View.VISIBLE
                    rv_temperature.visibility = View.GONE
                }

                else -> {
                    progress_bar.visibility = View.VISIBLE
                    rv_temperature.visibility = View.GONE
                }
            }
        })
    }
}