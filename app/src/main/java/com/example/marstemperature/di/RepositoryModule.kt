package com.example.marstemperature.di

import com.example.marstemperature.network.retrofit.ApiService
import com.example.marstemperature.repository.MarsTemperatureRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@InstallIn(ApplicationComponent::class)
@Module
object RepositoryModule {

    @Singleton
    @Provides
    fun providesMarsTemperatureRepository(
        retrofit: ApiService
    ): MarsTemperatureRepository {
        return MarsTemperatureRepository(retrofit = retrofit)
    }
}