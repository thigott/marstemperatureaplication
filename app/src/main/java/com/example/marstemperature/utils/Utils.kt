package com.example.marstemperature.utils

import java.text.SimpleDateFormat
import java.util.*


fun String.convertDate(): String {
    return try {
        val inputFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val outputFormatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val date = inputFormatter.parse(this)
        outputFormatter.format(date ?: Date())
    } catch (e: Exception){
        ""
    }
}

fun Double.formatTemperature(): Double {
    return String.format("%.2f", ((this - 32.0) * 5.0 / 9.0)).toDouble()
}


