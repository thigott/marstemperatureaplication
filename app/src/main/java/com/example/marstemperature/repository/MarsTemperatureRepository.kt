package com.example.marstemperature.repository

import com.example.marstemperature.network.retrofit.ApiService
import com.example.marstemperature.network.retrofit.MarsTemperatureRetrofitModel
import com.example.marstemperature.utils.DataState
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MarsTemperatureRepository
constructor(
    private val retrofit: ApiService
){

    fun getMarsTemperature(
        params: Map<String, String>
    ): Flow<DataState<ArrayList<MarsTemperatureRetrofitModel>>> = flow {
        try {
            emit(DataState.Loading)

            val result = retrofit.getMarsTemperature(params = params)

            val mapper = jacksonObjectMapper()
            val jsonResult = mapper.readValue<Map<String, Any?>>(result)

            val arrayTemperaturesModel = arrayListOf<MarsTemperatureRetrofitModel>()
            jsonResult.filter { it.key.toLongOrNull() != null }.forEach {
                arrayTemperaturesModel.add(
                    element = MarsTemperatureRetrofitModel.fromJson(Gson().toJson(it.value))
                )
            }

            emit(DataState.Success(arrayTemperaturesModel))
        } catch (e: Exception){
            emit(DataState.Error(e))
        }
    }

}